#Admin Preview

Version 1.0

This plugin keeps post previews within wp-admin.  Where normally the link for the preview would be 

http://example.com/page-slug/?preview_id=100&preview_nonce=abc&preview=true

now becomes

http://example.com/wp-admin/?preview=true&preview_nonce=abc&p=100&action=pageio_post_preview&nonce=def

This is useful when the front end is behind caching that strips the query paramaters, or the admin is hosted on a different domain.

## Notes

Since the template is usually included in the global scope, themes relying upon global variables beyond `$post` will need to add a filter attached to `admin-preview-globals`, returning an array of the names of global variables required.