<?php 

namespace Page_IO\Admin_Preview;

/**
*	sets up globals and renders page within admin
*	attached to `admin_action_pageio_post_preview` action
*	which is run on ?action=pageio_post_preview generated in preview_post_link()
*	@uses `admin-preview-globals` filter to account for custom globals needed
*/
function admin_preview(){
	$preview_id = filter_input( INPUT_GET, 'p', FILTER_SANITIZE_NUMBER_INT );
	$nonce = filter_input( INPUT_GET, 'nonce' );

	if( !$preview_id || !$nonce || !wp_verify_nonce($nonce, 'pageio_post_preview-'.$preview_id) )
		return;

	// this is the best I could come up with.  template-loader.php is normally included at the global scope
	$set_globals = apply_filters( 'admin-preview-globals', array('post') );
	foreach( $set_globals as $to_set )
		global $$to_set;

	add_filter('the_preview', '_set_preview');

	wp( array(
		'p' => $preview_id
	) );

	define( 'WP_USE_THEMES', TRUE );
	require_once ABSPATH . WPINC . '/template-loader.php';

	die();
}
add_action( 'admin_action_pageio_post_preview', __NAMESPACE__.'\admin_preview', 10 );

/**
*	creates the link used  in the 'Preview Changes' button in the Publish metabox
*	attached to `preview_post_link` filter
*	@param string
*	@param WP_Post
*	@return string
*/
function preview_post_link( $preview_link, $post ){
	$url = \sjr\parse_url( $preview_link );

	// preview_id is used for published posts, p is for draft posts
	if( isset($url['query']['preview_id']) ){
		$url['query']['p'] = $url['query']['preview_id'];
		unset( $url['query']['preview_id'] );
	}

	// so bad
	if( !isset($url['query']['p']) )
		$url['query']['p'] = $GLOBALS['post']->ID;

	$preview_link = \sjr\unparse_url( array(
		'host' => admin_url( '', $url['scheme'] ),
		'query' => array_merge( $url['query'], array(
			'action' => 'pageio_post_preview',
			'nonce' => wp_create_nonce( 'pageio_post_preview-'.$url['query']['p'] )
		) )
	) );

	// maybe need to unset ?preview_nonce

	return $preview_link;
}
add_filter( 'preview_post_link', __NAMESPACE__.'\preview_post_link', 1000, 2 );